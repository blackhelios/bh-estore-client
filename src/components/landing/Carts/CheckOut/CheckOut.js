import React, { Component } from 'react'
import TextInput from '../../../misc/forms/inputs/TextInput'
import { Field, reduxForm } from 'redux-form'
import TextArea from '../../../misc/forms/inputs/TextArea'

class CheckOut extends Component {




    render() {






        return (
            <div className="row">
                <div class="col-md-4 order-md-2 mb-4">
                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                        <span class="text-muted">Your cart</span>
                        <span class="badge badge-secondary badge-pill">3</span>
                    </h4>
                    <ul class="list-group mb-3">
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">Product name</h6>
                                <small class="text-muted">Brief description</small>
                            </div>
                            <span class="text-muted">$12</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">Second product</h6>
                                <small class="text-muted">Brief description</small>
                            </div>
                            <span class="text-muted">$8</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">Third item</h6>
                                <small class="text-muted">Brief description</small>
                            </div>
                            <span class="text-muted">$5</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between bg-light">
                            <div class="text-success">
                                <h6 class="my-0">Promo code</h6>
                                <small>EXAMPLECODE</small>
                            </div>
                            <span class="text-success">-$5</span>
                        </li>
                        <li class="list-group-item d-flex justify-content-between">
                            <span>Total (USD)</span>
                            <strong>$20</strong>
                        </li>
                    </ul>

                    <form class="card p-2">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Promo code" />
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-secondary">Redeem</button>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-md-8 order-md-1">
                    <h4 class="mb-3">Billing address</h4>
                    <form className="">
                        <Field
                            component={TextInput}
                            type="text"
                            placeholder="enter username"


                        />
                        <Field
                            component={TextInput}
                            type="text"
                            placeholder="enter phone"


                        />
                        <Field
                            component={TextArea}
                            type="text"
                            placeholder="enter address"
                            rows={5}

                        />
                        <div class="form-group form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1" />
                            <label class="form-check-label" for="exampleCheck1">Cash on Delivery if you checked . u don't need to proceed payment</label>
                        </div>
                        <button
                            className="btn btn-success"
                        >
                            Procced for payment
                        </button>

                    </form>
                </div>
            </div>




        )
    }
}




export default reduxForm({
    form: 'checkout'
})(CheckOut);