import React from 'react'
import './FooterBottom.css'


function FooterBottom() {
    return (
        <div id="footer-bottom">
            <div className="container-fluid">

                <div id="footer-copyrights">
                    <div>
                        <p>Copyrights &copy; 2019 All Rights Reserved by E-Store Shopping</p>
                    </div>
                    <div>
                        <span style={{ fontSize: '15px' }}>made with  <span style={{ color: '#e25555' }}>&hearts;</span>  by  <a style={{ textDecoration: 'none' }} href="https://www.facebook.com/blackheliosmm/">blackhelios</a></span>
                    </div>

                </div>

            </div>

        </div>

    )
}



export default FooterBottom;