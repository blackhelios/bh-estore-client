
export const navitems = [
    {
        name: 'home',
        linkTo: '/admin',
        icon: 'fa-home',
        admin: false
    },
    {
        name: 'products',
        linkTo: '/admin/products',
        icon: 'fa-archive',
        admin: false
    },

    {
        name: 'users',
        linkTo: '/admin/users',
        icon: 'fa-users',
        admin: false
    },
    {
        name: 'customers',
        linkTo: '/admin/customers',
        icon: 'fa-angellist',
        admin: false
    },

    {
        name: 'orders',
        linkTo: '/admin/orders',
        icon: 'fa-cart-plus',
        admin: false
    },

    {
        name: 'promotions',
        linkTo: '/admin/promotions',
        icon: 'fa-angellist',
        admin: false
    },

    {
        name: "site settings",
        linkTo: '/admin/settings',
        icon: 'fa-cog',
        admin: false
    }




]